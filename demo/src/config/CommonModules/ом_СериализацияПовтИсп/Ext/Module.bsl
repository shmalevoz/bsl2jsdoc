﻿// Кэш сериализации
//  

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает сериализатор XDTO на основании глобальной фабрики
// 
// Параметры: 
// 
// Возвращаемое значение: 
// 	СериализаторXDTO
// 
Функция Сериализатор() Экспорт
	
	#Если ВебКлиент Тогда
		ВызватьИсключение ом_СериализацияТексты.СредаСериализаторНедоступен();
	#Иначе
		Возврат ом_Сериализация.Сериализатор(ФабрикаXDTO);
	#КонецЕсли
	
КонецФункции // Сериализатор 

#КонецОбласти
