﻿// Проверка прав на доступ к администрированию адаптеров
//  

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Вызов исключения при недоступности администрирования адапетров
// 
// Параметры: 
// 
Процедура АдминистрированиеНедоступноИсключение() Экспорт
	
	Если НЕ ом_АдаптерНастройкаПовтИсп.РольАдминистрированиеДоступна() 
		И НЕ ПривилегированныйРежим()
		Тогда
		ВызватьИсключение ом_АдаптерТексты.ПраваАдминистрированиеНедоступно();
	КонецЕсли;
	
КонецПроцедуры // АдминистрированиеНедоступноИсключение 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
