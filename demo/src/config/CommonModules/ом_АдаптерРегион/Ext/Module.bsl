﻿// Регионы применения адаптеров - логически выделяемые области 
// 	использующие различные адаптеры одного вида в рамках 
// 	одной прикладной базы
//  

#Область ПрограммныйИнтерфейс

// Регистрация региона применения адаптеров
// 
// Параметры: 
// 	Вид - Строка - Идентификатор вида адаптеров
// 	Идентификатор - Строка - Идентификатор региона использования
// 	Наименование - Строка - Имя региона использования
// 
Процедура Добавить(Вид, Идентификатор, Наименование) Экспорт
	
	ом_АдаптерВызовСервера.РегионДобавить(Вид, Идентификатор, Наименование);
	
КонецПроцедуры // Добавить 

// Удаляет регистрацию региона применения адапетров
// 
// Параметры: 
// 	Вид - Строка - Идентификатор вида адаптеров
// 	Идентификатор - Строка - идентификатор области
// 
Процедура Удалить(Вид, Идентификатор) Экспорт
	
	ом_АдаптерВызовСервера.РегионУдалить(Вид, Идентификатор);
	
КонецПроцедуры // Удалить 

// Устанавливает наименование региона. Возвращает значение на момент до выполнения метода
// 	Если Наименование не задано, то значение не изменяется
// 
// Параметры: 
// 	Вид - Строка - Идентификатор вида адаптеров
// 	Идентификатор - Строка - идентификатор региона
// 	Наименование - Строка - устанавливаемое наименование
// 
// Возвращаемое значение: 
// 	Строка - наименование
// 
Функция Наименование(Вид, Идентификатор, Наименование = Неопределено) Экспорт
	
	Возврат ом_АдаптерВызовСервера.РегионНаименование(Вид, Идентификатор, Наименование);
	
КонецФункции // Наименование 

// Истина, если регион с идентификатором зарегистрирован
// 
// Параметры: 
// 	Вид - Строка - Идентификатор вида адаптеров
// 	Идентификатор - Строка - идентификатор области
// 
// Возвращаемое значение: 
// 	Булево
// 
Функция Зарегистрирован(Вид, Идентификатор) Экспорт
	
	Возврат ом_АдаптерВызовСервера.РегионЗарегистрирован(Вид, Идентификатор);
	
КонецФункции // Зарегистрирован 

// Возвращает коллекцию зарегистриированных регионов
// 
// Параметры: 
// 	Вид - Строка - Идентификатор вида адаптеров
// 
// Возвращаемое значение: 
// 	Соответствие - Ключ идентификатор, Значение наименование
// 
Функция Коллекция(Вид) Экспорт
	
	Возврат ом_АдаптерВызовСервера.РегионыКоллекция(Вид);
	
КонецФункции // Коллекция

// Возвращает идентификатор области по-умолчанию
// 
// Параметры: 
// 
// Возвращаемое значение: 
// 	Строка
// 
Функция Умолчание() Экспорт
	
	Возврат ом_АдаптерПовтИсп.РегионУмолчание();
	
КонецФункции // Умолчание 

// Истина, если передан реигон по-умолчанию
// 
// Параметры: 
// 	Регион - Строка, Ссылка - Идентификатор региона
// 
// Возвращаемое значение: 
// 	Булево
// 
Функция УмолчаниеЭто(Регион) Экспорт
	
	Возврат НЕ ЗначениеЗаполнено(Регион) ИЛИ Регион = Умолчание();
	
КонецФункции // УмолчаниеЭто 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает умолчание региона в случае неопределенности
// 
// Параметры: 
// 	Регион - Строка, Ссылка - Идентификатор региона
// 
// Возвращаемое значение: 
// 	Строка, Ссылка - в случае неопределенности переданного региона возвращает Умолчание
// 
Функция НеопределеноВУмолчание(Регион = Неопределено) Экспорт
	
	Возврат ом_АдаптерПовтИсп.РегионНеопределеноВУмолчание(Регион);
	
КонецФункции // НеопределеноВУмолчание 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
